# Movie scrapper monorepo

This repository holds references to the Backend Service and Frontend service of the Movie Scrapping Challenge

## Local Deployment

### Requirements
- `docker`
- `docker-compose`

In `Deployment/Local/` there is a `docker-compose.yml` file that builds the project.

Simply run
```
cd Deployment/Local
docker-compose up -d --build
```

The backend service uses a MongoDB database that is included in the file to hold information about the movies.
The cached images are persisted in a docker volume.

To access the provided Frontend service, you can access `http://localhost:8082`

A list of movies will appear. Clicking on each movie will provide additional information.
The cached images will have a `DOWNLOAD CACHED HERE` button next to them